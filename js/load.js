function init(){
  uploadFile = document.getElementById("file");
  uploadFile.addEventListener("change", loadFromClient);
  uploadFile.files[0] = ""
  console.log("ok")
  var canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");
  ctx.width = 480;
  ctx.height = 360;
}
function loadFromClient(){
  var file = uploadFile.files[0];
  var reader = new FileReader();

  reader.readAsText(file);
  reader.onload = function () {
    var rawJSON = reader.result;
    show(rawJSON)
    draw(rawJSON)
  }

}

function loadFromServer(){
  var path = document.getElementById("fileName").value

  fetch(path)
    .then(response => {
      response.text().then(text => {
        rawJSON = text;
        show(rawJSON)
        draw(rawJSON)
      });
    })
}

function show(rawJSON){
  var show = document.getElementById("show")

  show.textContent = rawJSON
  //console.log(rawJSON)
}
